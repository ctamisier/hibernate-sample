package dao;

import model.Book;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class BookDAO extends GenericDAO<Book> {

    private static BookDAO dao;

    private BookDAO() {
        super(Book.class);
    }

    public static BookDAO instance() {
        if (dao == null) {
            dao = new BookDAO();
        }
        return dao;
    }

    public List<Book> boughtBooks() {
        EntityManager entityManager = DatabaseHelper.createEntityManager();
        String qlQuery =
                "SELECT distinct b " +
                        "FROM Client c " +
                        "LEFT JOIN c.boughtBooks b";
        TypedQuery<Book> query = entityManager.createQuery(qlQuery, Book.class);
        return query.getResultList();
    }

}
