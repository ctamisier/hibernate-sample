package dao;

import model.Book;
import model.Client;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ClientDAO extends GenericDAO<Client> {

    private static ClientDAO dao;

    private ClientDAO() {
        super(Client.class);
    }

    public static ClientDAO instance() {
        if (dao == null) {
            dao = new ClientDAO();
        }
        return dao;
    }

    public Client findClientAndFetchBooks2Requests(Long id) {
        EntityManager entityManager = DatabaseHelper.createEntityManager();
        Client client = entityManager.find(Client.class, id);
        Hibernate.initialize(client.getBoughtBooks());
        entityManager.close();
        return client;
    }

    public Client findClientAndFetchBooks(Long id) {
        EntityManager entityManager = DatabaseHelper.createEntityManager();
        String qlQuery =
                "SELECT c " +
                        "FROM Client c " +
                        "LEFT JOIN FETCH c.boughtBooks " +
                        "WHERE c.id=:id";
        TypedQuery<Client> query = entityManager.createQuery(qlQuery, Client.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Book> booksBoughtBy2Requests(Long clientId) {
        EntityManager entityManager = DatabaseHelper.createEntityManager();
        List<Book> boughtBooks = entityManager.find(Client.class, clientId).getBoughtBooks();
        Hibernate.initialize(boughtBooks);
        return boughtBooks;
    }

    public List<Book> booksBoughtBy(Long clientId) {
        EntityManager entityManager = DatabaseHelper.createEntityManager();
        String qlQuery =
                "SELECT b " +
                        "FROM Client c " +
                        "INNER JOIN c.boughtBooks b " +
                        "WHERE c.id=:id";
        TypedQuery<Book> query = entityManager.createQuery(qlQuery, Book.class);
        query.setParameter("id", clientId);
        return query.getResultList();
    }
}
