import dao.BookDAO;
import dao.ClientDAO;
import model.Book;
import model.Client;

public class Run {
    public static void main(String[] args) {

        ClientDAO clientDAO = ClientDAO.instance();
        BookDAO bookDAO = BookDAO.instance();

        Client client1 = new Client("Dupont", "Jean", Client.Gender.M);
        Client client2 = new Client("Dupont", "Jeanne", Client.Gender.F);

        System.out.println(clientDAO.persist(client1));
        System.out.println(clientDAO.persist(client2));

        client1.setFirstName("Georges");
        System.out.println(clientDAO.merge(client1));
        System.out.println(clientDAO.find(1L));

        Book book1 = new Book("title1", "author1");
        Book book2 = new Book("title2", "author2");
        Book book3 = new Book("title3", "author3");
        Book book4 = new Book("title4", "author4");
        bookDAO.persist(book1);
        bookDAO.persist(book2);
        bookDAO.persist(book3);
        bookDAO.persist(book4);

        bookDAO.remove(book4.getId());

        client1.getBoughtBooks().add(book1);
        client1.getBoughtBooks().add(book2);

        client2.getBoughtBooks().add(book1);

        clientDAO.merge(client1);
        clientDAO.merge(client2);

        System.out.println(clientDAO.find(client1.getId()).getBoughtBooks().size());
        System.out.println(clientDAO.findClientAndFetchBooks2Requests(client1.getId()).getBoughtBooks().size());
        System.out.println(clientDAO.findClientAndFetchBooks(client1.getId()).getBoughtBooks().size());
        System.out.println(clientDAO.booksBoughtBy2Requests(client1.getId()).size());
        System.out.println(clientDAO.booksBoughtBy(client1.getId()).size());
        bookDAO.boughtBooks().stream().forEach(System.out::println);

    }

}


