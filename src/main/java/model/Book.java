package model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Book {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String author;

    @ManyToMany(mappedBy = "boughtBooks")
    private List<Client> buyers;

    @OneToMany(mappedBy = "favoriteBook")
    private List<Client> favoritesClient;

    public Book() {
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<Client> getBuyers() {
        return buyers;
    }

    public void setBuyers(List<Client> buyers) {
        this.buyers = buyers;
    }

    public List<Client> getFavoritesClient() {
        return favoritesClient;
    }

    public void setFavoritesClient(List<Client> favoritesClient) {
        this.favoritesClient = favoritesClient;
    }

    @Override
    public String toString() {
        return "id=" + id + ", title=" + title + ", author=" + author;
    }
}
